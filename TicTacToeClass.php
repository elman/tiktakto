<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicTacToeClass
 *
 * @author Emanuel Marques
 */
   class TicTacToe
   {
        protected $field=null;
        const THISFILENAME="index.php";
	const CSSFILENAME="TicTacToe.css";
	protected $textToTopPage=null;
	protected $winLine=null;

        function __construct()
        {
	    $this->field=array(' ',' ',' ',' ',' ',' ',' ',' ',' ');
	    $this->textToTopPage="Click in the grid";
	    $this->winLine=null;
        }
       
        function __destruct()
        {
	    unset($this->field);
	    unset($this->textToTopPage);
        }

        public function setfield($field)
        {
            $this->field=$field;
        }
	
	public function setCellInField($player,$index,&$field)
	{
	    $field[$index] = $player;
	    return TRUE;
	}
	/**
	 * protected function TicTacToe::isWinning(&$allFields)
	 *	    Verify if someone wins
	 * @param type $allFields
	 * @return char player
	 *	    'X'	=> Player X wins
	 *	    'O'	=> Player O wins
	 *	    '0' => (zero) no winner
	 */
        protected function isWinning(&$allFields)
        {
            $winCombinations = '012345678036147258048642';
	    $retval='0';
            //debug_print_backtrace();
            for($i=0;$i<=21;$i+=3)
            {
                $player = $allFields[$winCombinations[$i]];
		if((' ' != $player) && ($player == $allFields[$winCombinations[$i+1]]) && ($player == $allFields[$winCombinations[$i+2]]))
		{
		    $this->winLine=$winCombinations[$i].$winCombinations[$i+1].$winCombinations[$i+2];
		    $retval = $player;
		    break;
		}
            }
            return $retval;
        }
         
	/**
	 * protected function TicTacToe::isBlankCaseLeft(&$allFields)
	 *	    Verify if there is still an empty field 
	 * @param string $allFields
	 * @return boolean
	 *	    FALSE   => Grid is full
	 *	    TRUE    => It remains at least one Blank case
	 */
        protected function isBlankCaseLeft(&$allFields)
        {
            $isBlankLeft = FALSE;
            for ($i=0; $i<=8; $i++)
            {
                if (' ' === $allFields[$i])
                {
                    $isBlankLeft = TRUE;
                    break;
                }
            }
            return $isBlankLeft;
        }

	/**
	 * protected function TicTacToe::countXO(&$allFields)
	 * @param string $allFields
	 * @return boolean
	 *	    FALSE   => There are at least as much of O than X in grid
	 *	    TRUE    => There are more X than O in grid
	 */
	protected function countXO(&$allFields)
	{
	    $countX=0;
	    $countO=0;
	    for ($i=0; $i<=8; $i++)
            {
                if ('X' === $allFields[$i])
                {
                    $countX++;
                }
		if ('O' === $allFields[$i])
                {
                    $countO++;
                }
            }
	    return ($countX > $countO ? TRUE:FALSE);
	}

	/**
	 * protected function TicTacToe::playOpponent(&$allFields)
	 * @param string $allFields
	 * @return boolean
	 *	    FALSE   => could not play
	 *	    TRUE    => played successfully
	 */
        protected function playOpponent(&$allFields)
        {
	    $retval=FALSE;
	    if ((TRUE === $this->countXO($allFields)) && (TRUE === $this->isBlankCaseLeft($allFields)) && ('0' === $this->isWinning($allFields)))
	    {
		$i=4; // best case to fill is the one in the middle ...
		while ($allFields[$i] != ' ')
		{
		    $i=rand()%8;
		}
		// if setting is successufull inform on retval
		if (TRUE === $this->setCellInField('O',$i,$allFields))
		{
		    $retval=TRUE;
		}
	    }
            return $retval;
        }
        
	/**
	 * protected function TicTacToe::isGameTie(&$allFields)
	 * @param string $allFields
	 * @return boolean status
	 *	    FALSE   => game not finished
	 *	    TRUE    => game is Tie
	 */
        protected function isGameTie(&$allFields)
        {
            if ((FALSE === $this->isBlankCaseLeft($allFields)) && ('0' === $this->isWinning($allFields)))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
	/**
	 * protected function TicTacToe::gettingPreviousClick(&$allFields)
	 * @param string $allFields
	 * @return int POST status
	 *	    FALSE   => POST fail
	 *	    NULL    => no field in POST
	 *	    TRUE    => found field in POST
	 */
        protected function gettingPreviousClick(&$allFields)
        {
	    $retval=NULL;
	    for ($i=0; $i<=8; $i++)
	    {
		/**
		 * filter input : 
		 * returns :    FALSE => filter fail
		 *		    NULL  => variable not found
		 */
		$retval=filter_input(INPUT_POST,"field"."$i");
		if (($retval != FALSE) && ($retval != NULL))
		{
		    $retval = $this->setCellInField('X',$i,$allFields);
		    break;
		}
	    }
            return $retval;
        }
	/**
	 * protected function TicTacToe::havingResponse(&$allFields)
	 * @param type $allFields
	 * @return type information
	 *	    NULL    => could not make opponent play and is not Tie and X did not win
	 *			(this happens only if user tries to refresh page without playing)
	 *	    FALSE   => updated info for Tie Game
	 *	    TRUE    => updated info for winner Game
	 */
        protected function havingResponse(&$output,&$allFields)
        {
	    $retval = FALSE;
            if (TRUE === $this->printOutIfPlayerWon($output,'X',$allFields))
	    {
		$retval = TRUE;
		return $retval;
	    }
            if ((FALSE === $this->playOpponent($allFields)) && ($retval == FALSE))
	    {
		$retval = NULL;
	    }
            if (TRUE === $this->printOutIfPlayerWon($output,'O',$allFields))
	    {
		$retval = TRUE;
		return $retval;
	    }
            if (TRUE === $this->isGameTie($allFields))
            {
                $output = "Game is Tie";
		$retval = FALSE;
            }
            return $retval;
        }
        
	/**
	 * protected function TicTacToe::printOutIfPlayerWon(&$output,$player,&$allFields)
	 *	Informs user about result on Table Header
	 * @param string $output
	 * @param char $player
	 * @param type $allFields
	 * @return int status
	 *	    FALSE   => nothing changed
	 *	    TRUE    => TH has been updated with game result
	 */
        protected function printOutIfPlayerWon(&$output,$player,&$allFields)
        {
	    $retval=FALSE;
            if ($player === $this->isWinning($allFields))
            {
                $output = "<p>".$player." wins</p>";
		$retval = TRUE;
            }
	    return $retval;
        }
	
	/**
	 * public function TicTacToe::SessionHandler()
	 *	Manages the session and variables
	 * @return type status
	 *	    NULL    => session is new and empty
	 *	    FALSE   => failed to start session
	 *	    TRUE    => session could be handled
	 */
        public function SessionHandler()
        {
	    $retval = session_start();
            if ("Nouveau Jeu" === filter_input(INPUT_POST,"TheButton"))
            {
                session_destroy();
                $this->setfield(array(' ',' ',' ',' ',' ',' ',' ',' ',' '));
                header("Refresh:0; url=".self::THISFILENAME);
            }
            if (NULL != $_SESSION)
            {
                if ('' != $_SESSION["field"])
                {
                    $this->field = $_SESSION["field"];
                }
                else 
                {
                    $_SESSION["field"] = $this->field;
                }
                $this->gettingPreviousClick($this->field);
                $this->havingResponse($this->textToTopPage,$this->field);
		$retval = TRUE;
            }
	    else 
	    {
		$retval = NULL;
	    }
            $_SESSION["field"] = $this->field;
	    return $retval;
        }
   }
   
   class TicTacToeLayout extends TicTacToe
   {
	/**
	 * protected function TicTacToe::fillCell($cellIndex,&$allfields)
	 *	    HTML page filling a cell in grid
	 * @param int $cellIndex
	 * @param type $allFields
	 * @return boolean status
	 *	    FALSE	=> cell filled with X or O letter
	 *	    TRUE	=> cell filled with button
	 */
        protected function fillCell($cellIndex,&$allfields)
        {
	    $retval = NULL;
            if (' ' === $allfields[$cellIndex])
            {
                //If someone won, the buttons should not be clickable
                if('0' === $this->isWinning($allfields))
                {
                   printf('<input type="submit" name="field%s" value="%s">',$cellIndex,$allfields[$cellIndex]);
		   $retval = TRUE;
                }
                else 
                {
                    printf('<p id="grid">%s</p>',$allfields[$cellIndex]);
		    $retval = FALSE;
                }
            }
            else 
            {
                printf('<p id="grid">%s</p>',$allfields[$cellIndex]);
		$retval = FALSE;
            }
	    return $retval;
        }
        
	/**
	 * protected function TicTacToe::displayBottom(&$allFields)
	 *	    HTML page lower level display
	 * @param type $allFields
	 * @return boolean status
	 *	    FALSE	=> play demand
	 *	    TRUE	=> button for new game
	 */
        protected function displayBottom(&$allfields)
        {
            // Providing buttons for the page
	    $retval = NULL;
            if (('X' != $this->isWinning($allfields)) && ('O' != $this->isWinning($allfields)) && (FALSE === $this->isGameTie($allfields)))
            {
                printf('<p>play!</p>');
		$retval = FALSE;
            }
            else
            {
                printf('<input type="submit" name="TheButton" value="%s" id="TheButton" onclick="window.location.href=\'%s\'">',"Nouveau Jeu",self::THISFILENAME);
		$retval = TRUE;
            }
	    return $retval;
        }
        
	/**
	 * protected function TicTacToe::displayUnitInTable($index,&$winLine,&$allfields)
	 *	    HTML page Table element display
	 * @param int $index
	 * @param type $winLine
	 * @param type $allFields
	 * @return boolean status
	 *	    FALSE	=> element is green, not in winner line
	 *	    TRUE	=> element is colored for winner line
	 */
	protected function displayUnitInTable($index,&$winLine,&$allfields)
	{
	    $retval = FALSE;
	    if ((NULL != $winLine) && (($index == $winLine[0] || $index == $winLine[1] || $index == $winLine[2])))
	    {
		printf('<td id="pink">');
		$retval = TRUE;
	    }
	    else
	    {
		printf('<td id="green">');
	    }
	    $this->fillCell($index,$allfields);
	    printf('</td>');
	    return $retval;
	}
	/**
	 * protected function TicTacToe::displayTable(&$winLine,&$allfields)
	 *	    HTML page game grid display
	 * @param type $winLine
	 * @param type $allFields
	 * @return boolean status
	 *	    FALSE	=> grid is all green
	 *	    TRUE	=> grid is colored for winner line
	 */
        protected function displayTable(&$winLine,&$allfields)
        {
            // Generating lines and rows for the Game
	    $retval = FALSE;
            printf('<table>');
	    printf('<th colspan="3">%s</th>',$this->textToTopPage);
            for ($i=0; $i<=8; $i++)
            {
                if (0 === $i || 3 === $i || 6 === $i)
                {
                    printf('<tr>');
                }
		if ($this->displayUnitInTable($i,$winLine,$allfields) == TRUE)
		{
		    $retval = TRUE;
		}
		if (2 === $i || 5 === $i || 8 === $i)
                {
                    printf('</tr>');
                }
            }
            printf('</table>');
	    return $retval;
        }
        // Provides appearance of html page
        public function Display()
        {
            // Begin of Display, opening page :
            printf('<html><head><link rel="stylesheet" type="text/css" href="%s" /><title>Jeu de morpion</title></head><body>',self::CSSFILENAME);

            // open form
            printf('<form name="morpion" method="post" action="%s">',self::THISFILENAME);

	    $this->displayTable($this->winLine,$this->field);
            $this->displayBottom($this->field);
            // Closing form
            printf('</form>');
            // End of Display, close page :
            printf('</body></html>');
            return 0;
        }
   }
?>